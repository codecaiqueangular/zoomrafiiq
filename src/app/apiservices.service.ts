import { Injectable } from '@angular/core';
import { HttpClient, HttpParams, HttpHeaders } from '@angular/common/http'
import { Observable, of, } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { Time } from '@angular/common';
const httpOptions = {
  headers: new HttpHeaders({ 'content': 'application/json' })
}
@Injectable({
  providedIn: 'root'
})
export class ApiservicesService {

  constructor(private http: HttpClient) { }
  
     // showMeeting
     getMeeting(meetingNumber: number,role: number,api_key:string,api_secret:string): Observable<any[]> {
      const formData: FormData = new FormData();
  
      
      formData.append('meetingNumber', meetingNumber+"");
      formData.append('role', role+"");
      formData.append('api_key', api_key);
      formData.append('api_secret', api_secret);
  
      return this.http.post<any[]>('https://rafikapi.codecaique.com/api/getMeeting', formData)
  
  
}


       

}
