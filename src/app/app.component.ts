import { Component, OnInit, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { ApiservicesService } from './apiservices.service';

import { ZoomMtg } from '@zoomus/websdk';
// import { checkServerIdentity } from 'tls';
// import { loadavg } from 'os';

ZoomMtg.preLoadWasm();
ZoomMtg.prepareJssdk();

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  [x: string]: any;
  id;
  pass;
  pstn_password;

  apiKey;
  apiSecret;
  meetingNumber;
  role;
  leaveUrl;
  userName;
  userEmail;
  passWord;
  signature;
  //84243603767
  insta_link;
  face_link;
  type;

  ImageURl = 'rafikapi.codecaique.com';

  constructor(
    public _service: ApiservicesService,
    @Inject(DOCUMENT) document,
    private route: ActivatedRoute
  ) {}

  ngOnInit() {
    //show_someNews//

    this.route.queryParams.subscribe((params) => {
      this.meetingNumber = params['id'];
      this.pass = params['password'];
      this.join_url = params['join_url'];
      this.start_url = params['start_url'];
      this.type = params['type'];
      this.pstn_password = params['pstn_password'];

      this.apiKey = 'Y3UpCvRpRM--m6HEA_0SpQ';
      this.apiSecret = '2RAbm1uKmGazFE0qmpTRlYKLO3xkXZ9uPpXL';
      // this.meetingNumber = 86373140056;
      this.role = 1;
      this.leaveUrl = 'https://zoom.rafiiq.net/';
      this.userName = 'Rafiiq';
      this.userEmail = 'apprafiiq@gmail.com';
      this.passWord = 'Rafiq24680';

      ZoomMtg.preLoadWasm();
      ZoomMtg.prepareJssdk();
      this.check(this.meetingNumber, this.type);
      console.log(this.meetingNumber);
      console.log(this.pass);
    });
  }

  check(meetingnumber, type) {
    if (this.meetingNumber == null) {
      console.log('meeting number == null ');
    } else {
      console.log('meeting number != null ');
    }
    if (this.type == null) {
      console.log('type == null ');
    } else {
      console.log(' type != null ');
    }

    console.log(this.meetingNumber, this.type);
  }

  getSignature() {
    this.check(this.meetingNumber, this.role);
    this._service
      .getMeeting(this.meetingNumber, this.role, this.apiKey, this.apiSecret)
      .subscribe((data) => {
        let resources: any = data['data'];
        this.signature = resources['signature'];
        console.log('signature', this.signature);

        this.startMeeting(this.signature);
      });
  }

  startMeeting(signature) {
    document.getElementById('zmmtg-root').style.display = 'block';

    ZoomMtg.init({
      leaveUrl: this.leaveUrl,
      isSupportAV: true,
      success: (success) => {
        console.log(success);

        ZoomMtg.join({
          signature: signature,
          meetingNumber: this.meetingNumber,
          userName: 'Rafiiq',
          apiKey: this.apiKey,
          success: (success) => {
            console.log(success);
          },
          error: (error) => {
            console.log(error);
          },
        });
      },
      error: (error) => {
        console.log(error);
      },
    });
  }

  check_data(d: String) {
    const meetConfig = {
      apiKey: '3239845720934223459',
      meetingNumber: '123456789',
      leaveUrl: 'https://yoursite.com/meetingEnd',
      userName: 'Firstname Lastname',
      userEmail: 'firstname.lastname@yoursite.com',
      passWord: 'password', // if required
      role: 0, // 1 for host; 0 for attendee
    };
  }
}
